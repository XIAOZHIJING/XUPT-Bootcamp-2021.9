cmake_minimum_required(VERSION 3.5)

project(server LANGUAGES C)

add_executable(server main.c)

target_link_libraries(server pthread jpeg)
