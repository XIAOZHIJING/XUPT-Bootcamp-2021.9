#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QNetworkAccessManager>
#include <QNetworkReply>

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();
    void sendRequest();

private slots:
    void responseReceived();
    void on_pushButton_clicked(bool checked);

private:
    Ui::Widget *ui;
    QNetworkAccessManager client;
    QNetworkReply* response;
    bool stop;
};
#endif // WIDGET_H
