## V4L2 API

Video for Linux version 2 API

API文档：https://www.kernel.org/doc/html/latest/userspace-api/media/v4l/v4l2.html

头文件：<linux/videodev2.h>

1. 打开摄像头设备文件（读写方式）

```C
int open("/dev/video0", O_RDWR);
```

- 返回值：文件描述符（文件句柄），0或者正数，失败返回-1

2. 查询设备能力（设备类型）

```C
int ioctl(int fd, VIDIOC_QUERYCAP, struct v4l2_capability *argp);
```

- fd：open返回的文件描述符

判断`struct v4l2_capability`结构体中的capabilities成员对应的标志位是否置位。

3. 查询设备支持的默认图像格式（可选，分辨率和颜色空间）

```C
int ioctl(int fd, VIDIOC_G_FMT, struct v4l2_format *argp);
```

4. 设置使用的图像格式（可选）

```C
int ioctl(int fd, VIDIOC_S_FMT, struct v4l2_format *argp);
```

5. 分配缓冲区

```C
int ioctl(int fd, VIDIOC_REQBUFS, struct v4l2_requestbuffers *argp);
```

6. 查询缓冲区地址并映射到用户空间

```C
int ioctl(int fd, VIDIOC_QUERYBUF, struct v4l2_buffer *argp);
mmap(NULL, buffer.length, PROT_READ | PROT_WRITE, MAP_SHARED, fd, buffer.m.offset);
```

7. 将缓冲区入队

```C
int ioctl(int fd, VIDIOC_QBUF, struct v4l2_buffer *argp);
```

8. 启动串流，摄像头驱动会将捕获的一帧图像保存到缓冲区中

```C
int ioctl(int fd, VIDIOC_STREAMON, const int *argp);
```

9. 等待驱动填充缓冲区，并将缓冲区出队。（阻塞）

```C
int ioctl(int fd, VIDIOC_DQBUF, struct v4l2_buffer *argp);
```

10. 处理缓冲区中的一帧图像
11. 跳转到步骤7 循环执行
12. 停止串流，关闭摄像头

```C
int ioctl(int fd, VIDIOC_STREAMOFF, const int *argp);
int close(int fd);
```

