- Qt 开发环境：https://mirrors.tuna.tsinghua.edu.cn/qt/official_releases/qt/5.12/5.12.11/
  - 最新版本的离线安装包，需要断网安装，否则会提示注册Qt账号。

- 虚拟机：https://www.virtualbox.org/wiki/Downloads platform packages 和 Extension Pack 都需要下载安装

- debian安装盘：https://mirrors.tuna.tsinghua.edu.cn/debian-cd/current/amd64/iso-cd/debian-11.0.0-amd64-netinst.iso



虚拟机安装完成后，修改网卡配置为桥接模式，分配内网IP地址，方便远程登录。

![image-20210913162157482](VM%20Setup.assets/image-20210913162157482.png)

安装远程登录客户端（PuTTY）：https://the.earth.li/~sgtatham/putty/latest/w64/putty-64bit-0.76-installer.msi

安装文件传输客户端（WinSCP）：https://winscp.net/download/WinSCP-5.19.2-Setup.exe



查看虚拟机IP地址：`ip address`

配置PuTTY远程连接虚拟机或开发板

![image-20210913163355685](VM%20Setup.assets/image-20210913163355685.png)

使用WinSCP连接虚拟机或开发板传输文件

![image-20210913163825384](VM%20Setup.assets/image-20210913163825384.png)



